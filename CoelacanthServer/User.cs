﻿using System;
using System.Text;
using System.Net.Sockets;

namespace CoelacanthServer
{
    class User
    {
        /* ---------------------------------------------
         * 1. 유저의 버퍼, 데이터, 소켓 정보를 저장할 클래스 변수 생성
         * 2. 정보 조회에 필요한 데이터 지정 (DB 추가되면, 따로 가져올 예정)
         * 3. 호스트와 게스트 구분 지정
         * 4. 핵 지정
        --------------------------------------------- */
        string nick;
        UserData data = new UserData(); // 소켓, 버퍼, 데이터 길이 등을 저장할 클래스 변수를 생성한다.
   
        int win = 0, lose = 0;
        User hostUser = null, guestUser = null;
        int REDnucleus, BLUEnucleus;

        public User(Socket socket)
        {
            data.workSocket = socket; // UserData의 workSocket를 서버에 연결된 소켓으로 설정한다.

            // 비동기 소켓 리시브를 실행한다. 클라이언트에서 데이터가 도착하면 ReadCallback이 자동으로 호출된다.
            data.workSocket.BeginReceive(data.buffer, data.recvlen, UserData.BufferSize, 0, new AsyncCallback(ReadCallback), data);
            WriteLine("CONNECT"); // 유저가 접속했을때 곧바로 클라이언트로 보내지는 패킷
        }

        /* ---------------------------------------------
         * 유저가 소켓에 접속하면서 대리자 비동기 작업을 시작한다.
         * 완료가 되었을 때, 콜백이 되는데 인자를 넘겨준다.
         * [참고]
         * http://blog.daum.net/creazier/15309346
         * https://msdn.microsoft.com/ko-kr/library/wyd0d1e5(v=vs.100).aspx (콜백 기술을 사용하여 비동기 웹 서비스 클라이언트 구현)
         * https://msdn.microsoft.com/ko-kr/library/system.asynccallback(v=vs.110).aspx (AsyncCallback 대리자)
         * 
         * # REMARKS
         * 유저가 접속한 뒤, 처리방법을 잘 모르겠어서 검색을 많이 해봤다.
         * 데이터를 어떻게 길이를 지정하는지
         * 데이터 길이를 왜 구해야 하는지
         * byte형식의 변환이 왜 필요한지
         * 2바이트의 검증 데이터와 실제 데이터를 나눠서 왜 같이 보내는지 등
         * 이외에도 궁금한게 많지만, 정상 작동이 되는것을 목표로 두고
         * 부족한 부분은 나중에 공부
        --------------------------------------------- */
        void ReadCallback(IAsyncResult ar)
        {
            try
            {
                Socket handler = data.workSocket;
                int bytesRead = handler.EndReceive(ar);

                if (bytesRead > 0) {
                    data.recvlen += bytesRead;

                    while (true)
                    {
                        short length;
                        Util.GetShort(data.buffer, 0, out length);

                        if (length > 0 && data.recvlen >= length)
                        {
                            ParsePacket(length);
                            data.recvlen -= length;

                            if (data.recvlen > 0)
                                Buffer.BlockCopy(data.buffer, length, data.buffer, 0, data.recvlen);
                            else {
                                handler.BeginReceive(data.buffer, data.recvlen, UserData.BufferSize, 0, new AsyncCallback(ReadCallback), data);
                                break;
                            }
                        }
                        else {
                            handler.BeginReceive(data.buffer, data.recvlen, UserData.BufferSize, 0, new AsyncCallback(ReadCallback), data);
                            break;
                        }
                    }
                }
                else {
                    handler.BeginReceive(data.buffer, data.recvlen, UserData.BufferSize, 0, new AsyncCallback(ReadCallback), data);
                }
            }

            catch (Exception)
            {
                Disconnect();
                Server.DeleteUser(this);
                Console.WriteLine("Socket Closed.");
            }
        }

        private void ParsePacket(int length)
        {
            // Equals("CONNECT") : 클라이언트가 서버에 접속할 경우
            // Equals("DISCONNECT") : 클라이언트가 접속을 끊었을 경우

            string msg = Encoding.UTF8.GetString(data.buffer, 2, length - 2);
            string[] text = msg.Split(':');

            if (text[0].Equals("CONNECT"))
            {
                nick = text[1];
                Console.WriteLine(text[1] + " 님이 접속하셨습니다.");
                WriteLine(string.Format("USER:{0}:{1}", win, lose));
            }
            else if (text[0].Equals("DISCONNECT"))
            {
                if (nick.Length > 0)
                {
                    Console.WriteLine(nick + " 님이 종료하셨습니다.");
                }
                data.workSocket.Shutdown(SocketShutdown.Both);
                data.workSocket.Close();
            }
        }

        byte[] ShortToByte(int val)
        {
            byte[] temp = new byte[2];
            temp[1] = (byte)((val & 0x0000ff00) >> 8);
            temp[0] = (byte)((val & 0x000000ff));
            return temp;
        }

        void WriteLine(string text)
        {
            try
            {
                // 서버에 패킷 정보를 보낸다.
                if (data.workSocket != null && data.workSocket.Connected)
                {
                    byte[] buff = new byte[4096];
                    // 1. 문자열의 2바이트를 버퍼에 넣는다.
                    // 2. 문자열을 byte로 형변환해서 버퍼에 추가한다.
                    // 3. 문자열 크기와 2바이트 정보를 소켓으로 전송한다.
                    Buffer.BlockCopy(ShortToByte(Encoding.UTF8.GetBytes(text).Length + 2), 0, buff, 0, 2);
                    Buffer.BlockCopy(Encoding.UTF8.GetBytes(text), 0, buff, 2, Encoding.UTF8.GetBytes(text).Length); 
                    data.workSocket.Send(buff, Encoding.UTF8.GetBytes(text).Length + 2, 0); 
                }
            }

            catch (System.Exception ex)
            {
                Disconnect();
                // 1. 접속이 원활하지 않을 경우, 소켓을 닫는다.
                data.workSocket.Shutdown(SocketShutdown.Both);
                data.workSocket.Close();

                Server.DeleteUser(this);
                Console.WriteLine("WriteLine Error : " + ex.Message);
            }
        }

        void Disconnect()
        {
            if (nick.Length > 0)
            {
                if (hostUser != null || guestUser != null)
                {
                    // 내가 방에 입장했을 경우,
                    if (hostUser != this) // hostUser가 다른 사람이면 게스트로 입장한 상태
                    {
                        hostUser.WriteLine("OUT"); // 게스트가 나감
                        hostUser.guestUser = null; // 다른 유저가 접속할 수 있도록 초기화
                    }
                    // 내가 호스트일 경우,
                    else
                    {
                        if (guestUser != null)
                        {
                            // 게스트 퇴장을 서버에 알림
                            guestUser.WriteLine("OUT");
                            guestUser.hostUser = this;
                            guestUser.guestUser = null;
                        }
                    }
                }
            }
        }
    }
}
